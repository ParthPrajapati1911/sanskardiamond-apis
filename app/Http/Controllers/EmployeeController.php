<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Dailywork;
use App\Department;
use App\Http\Controllers\Functions;
use Hash;
use App\User;
use Auth;
use App\TestTable;
use Config;

class EmployeeController extends Controller
{
    use Functions;

    /* Login employee with mobile no and password */
    public function login(Request $request)
    {
        $v = validator($request->all(), [
            'mobile_no' => 'required|max:10',
            'password' => 'required',
            // 'device_token' => 'required|max:200',
        ]);
        if ($v->fails()) return $this->sendResponse(true,$v->errors()->first());
        $employee = Employee::
        where('employee_phone',$request->mobile_no)->first();
        
        if(!$employee) return $this->sendResponse(true,"Invalid Employee, please try again!.",Config::get('constants.FAILED_RESPONSE'));
        if(md5($request->password)!=$employee->password) return $this->sendResponse(true,"Invalid Creditionals, please try again!.",Config::get('constants.FAILED_RESPONSE'));
        // $user->device_token = $request->device_token;
        // $user->device_type = $request->device_type;
        // $user->save();

        // $userTokens = $user->tokens;
        // foreach($userTokens as $token) {
        //     $token->revoke();
        // }
        $user = User::where('employee_id',$employee->employee_id)->first();
        if(!$user){
            // dd(Auth::attempt(['employee_phone' => $request->mobile_no, 'password' => $request->password]));
            // Auth::attempt(['employee_phone' => request('email'), 'password' => request('password')]);
            $input['name']=$employee->employee_name;
            $input['employee_phone']=$employee->employee_phone;
            $input['email']='';
            $input['password']=md5($request->password);
            $input['employee_id']=$employee->employee_id;
            User::create($input);
        }
        $user = User::where('employee_id',$employee->employee_id)->first();
        $accessTokenObject = $user->createToken('Login');
        $current_month = date('F');
        $month = date('M');
        $present_days = Dailywork::where('employee_id',$employee->employee_id)
        ->where('month',$month)
        ->count();
        $current_month_diamond = Dailywork::where('employee_id',$employee->employee_id)
                    ->where('month',$month)
                    ->sum('today_diamond');
        $department = Department::where('department_id',$employee->employee_department)->first();
        $employee->employee_department = $department->department_name;
        $current_month_work = $current_month_diamond*$department->department_rate;
        $data = [
            'employee' => $employee,
            'current_month' => $current_month,
            'currrent_month_present_days' => $present_days,
            'current_month_diamond' => $current_month_diamond,
            'current_month_work'=> $current_month_work,
            'token' => $accessTokenObject->accessToken
        ];

        // $accessToken = auth()->user()->token();
        // \DB::table('oauth_refresh_tokens')
        //     ->where('access_token_id', $accessToken->id)
        //     ->update([
        //         'revoked' => true
        //     ]);
        // $accessToken->revoke();
        
        return $this->sendResponse(true,'success',Config::get('constants.SUCCESS_RESPONSE'),$data);
    }

    /* This api for get logged in user all month work data */
    public function monthWorkAnalysis(){
        $dailywork = Dailywork::selectRaw('SUM(today_diamond) as diamond, month')
        ->where('employee_id',auth()->user()->employee_id)
        ->groupBy('month')
        ->get()->toArray();

        $employee = Employee::where('employee_id',auth()->user()->employee_id)->first();
        $department = Department::where('department_id',$employee->employee_department)->first();
        $department_rate = $department->department_rate;
        $i = 0;
        foreach($dailywork as $data){
            $dailywork[$i]['total_work'] = $data['diamond']*$department_rate;
            $i++;
        }
        $current_month = date('F');
        $month = date('M');
        
        $present_days = Dailywork::where('employee_id',auth()->user()->employee_id)
        ->where('month',$month)
        ->count();
        $current_month_diamond = Dailywork::where('employee_id',$employee->employee_id)
                    ->where('month',$month)
                    ->sum('today_diamond');
        $department = Department::where('department_id',$employee->employee_department)->first();
        $current_month_work = $current_month_diamond*$department->department_rate;
        $data = [
            'work_analysis' => $dailywork,
            'current_month' => $current_month,
            'currrent_month_present_days' => $present_days,
            'current_month_diamond' => $current_month_diamond,
            'current_month_work'=> $current_month_work,
        ];
        return $this->sendResponse(true,'success',Config::get('constants.SUCCESS_RESPONSE'),$data);
    }

    /** Update profile of logged in user */
    public function updateProfile(Request $request)
    {
        $v = validator($request->all(), [   
            'name' => 'required'
        ]);
        if ($v->fails()) return $this->sendResponse(true,$v->errors()->first());
        $user = auth()->user();
        if(!$user) return $this->sendResponse(true,'User not found.'); 
        $user->name = $request->name;
        $user->save();       
        return $this->sendResponse(true,'success',Config::get('constants.SUCCESS_RESPONSE'),$user);
    }

    /** Update profile of logged in user */
    public function monthBaseData(Request $request)
    {
        $dailywork = Dailywork::query();
        if($request->year){
            $dailywork = $dailywork->whereYear('date', '=', $request->year);
        }
        if($request->month){
            $dailywork = $dailywork->where('month',$request->month);
        }
        
        $dailywork = $dailywork->where('employee_id',auth()->user()->employee_id)
        ->select('dailywork_id','employee_id','date','month','today_diamond','total_diamond')
        ->get()->toArray();

        $employee = Employee::where('employee_id',auth()->user()->employee_id)->first();
        $department = Department::where('department_id',$employee->employee_department)->first();
        $department_rate = $department->department_rate;
        $i = 0;
        foreach($dailywork as $data){
            $dailywork[$i]['department_rate'] = $department_rate;
            $dailywork[$i]['day_work'] = $data['today_diamond']*$department_rate;
            $dailywork[$i]['total_work'] = $data['total_diamond']*$department_rate;
            $i++;
        }
        $dailywork1 =  new class {};
        $dailywork1->month_base_data = $dailywork;
        return $this->sendResponse(true,'success',Config::get('constants.SUCCESS_RESPONSE'),$dailywork1);
    }

    /** Change password of logged in user */
    public function changePassword(Request $request)
    {
        $v = validator($request->all(), [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($v->fails()) return $this->sendResponse(true,$v->errors()->first());

        $user = auth()->user();
        if(!$user) return $this->sendResponse(true,'User not found.',Config::get('constants.FAILED_RESPONSE'));
        if(md5($request->old_password)!=$user->password) return $this->sendResponse(true,'Incorrect Old Password',Config::get('constants.FAILED_RESPONSE'));
        if(md5($request->password)==$user->password) return $this->sendResponse(true,'New Password is same as a old password',Config::get('constants.FAILED_RESPONSE'));
        
        $employee = Employee::where('employee_id',$user->employee_id);
        $update['password'] = md5($request->password);
        $user->password = md5($request->password);
        $employee->update($update);
        $user->save();
        
        return $this->sendResponse(true,'Successfully changed password',Config::get('constants.SUCCESS_RESPONSE'));
    }

    /** Logout user */
    public function logout() {
        $accessToken = Auth::user()->token();
        \DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        return $this->sendResponse(true,'Successfully logout',Config::get('constants.SUCCESS_RESPONSE'));
    }

    /**For get month base first five winners work base */
    public function getWinners(Request $request){
        $v = validator($request->all(), [
            'month' => 'nullable'
        ]);
        if ($v->fails()) return $this->sendResponse(true,$v->errors()->first());
            
        if($request->month){
            $month = $request->month;
        }else{
            $month = date('M');
        }
        $dailywork = Dailywork::selectRaw('employee_id,SUM(today_diamond) as diamond')
        ->with('employee_details')
        ->where('month',$month)
        ->groupBy('employee_id')
        ->take(5)
        ->orderBy('diamond','desc')
        ->get()->toArray();

        $i = 0;
        foreach($dailywork as $data){
            $employee = Employee::where('employee_id',$data['employee_id'])->first();
            $department = Department::where('department_id',$employee->employee_department)->first();
            $department_rate = $department->department_rate;

            $dailywork[$i]['department_rate'] = $department_rate;
            $dailywork[$i]['total_work'] = $data['diamond']*$department_rate;
            $i++;
        }
        $dailywork1 =  new class {};
        $dailywork1->get_winners = $dailywork;
        return $this->sendResponse(true,'success',Config::get('constants.SUCCESS_RESPONSE'),$dailywork1);
    }
}
