<?php

namespace App\Http\Controllers;
use Config;
use Illuminate\Http\Request;

trait Functions
{
    // send json response
    public function sendResponse($status, $message,$status_code=null, $data=null)
    {
        // dd($status, $message,$status_code, $data);
        if($data==null){
            $data = Config::get('constants.BLANK_JSON_RESPONSE');
        }
        if($status_code==null){
            $status_code = Config::get('constants.FAILED_RESPONSE');
        }
        // if ($status && $status_code) {
            return response()->json(['message'=>$message,'data'=>$data, "status_code"=> $status_code ],200);
        // }else{
        //     return response()->json(['error'=>$message,'data'=>$data, "status_code"=> $status_code], 400);
        // }
    }
}
