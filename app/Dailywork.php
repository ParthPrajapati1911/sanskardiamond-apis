<?php

namespace App;
use App\Employee;
use Illuminate\Database\Eloquent\Model;

class Dailywork extends Model
{
    protected $table = 'dailywork';
    protected $fillable = ['employee_id','date','month','today_diamond','total_diamond','paid'];

    /* relations */
    public function employee_details()
    {
        return $this->hasOne(Employee::class, 'employee_id','employee_id');
    }
}

