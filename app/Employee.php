<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Model
{
    use Notifiable,HasApiTokens;
    protected $table = 'employee';
    protected $fillable = [
        'employee_id','password','employee_department','updated_at'
    ];

    public function department_details(){
        return $this->hasOne(Department::class,'department_id','employee_department');
    }
}
