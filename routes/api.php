<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvsider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('employee')->group(function(){
    Route::post('login','EmployeeController@login');
    // Route::post('registration','EmployeeController@registration');
    Route::post('forgetPassword','EmployeeController@forgetPassword');
    Route::post('resetPassword','EmployeeController@resetPassword');
    Route::post('changePassword','EmployeeController@changePassword')->middleware('auth:api');
    Route::post('updateProfile','EmployeeController@updateProfile')->middleware('auth:api');
    Route::post('logout','EmployeeController@logout')->middleware('auth:api');

    Route::post('monthWorkAnalysis','EmployeeController@monthWorkAnalysis')->middleware('auth:api');
    Route::post('getWinners','EmployeeController@getWinners')->middleware('auth:api');
    Route::post('monthBaseData','EmployeeController@monthBaseData')->middleware('auth:api');
    Route::post('logout','EmployeeController@logout')->middleware('auth:api');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

